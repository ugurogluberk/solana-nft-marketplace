const mongoose = require('mongoose');

// Connect to MongoDB
mongoose.connect('mongodb://localhost:27017/solanex', { useNewUrlParser: true }, (err) => console.log(err));

// Create a schema for the user data
const userSchema = new mongoose.Schema({
    profilePicture: String,
    userName: String,
    aboutYou: String,
    webSite: String,
    pubKey: String
});

const User = mongoose.model('User', userSchema);


module.exports = User;


