const restify = require('restify');
const User = require('./DatabaseStuff/database.js')

const server = restify.createServer();

server.use(restify.plugins.bodyParser({
    mapParams: true,
}));


server.get('/get/:pubKey', (req, res, next) => {
    const pubKey = req.params.pubKey;
    User.findOne({ pubKey: pubKey }, (err, user) => {
        if (err) {
            res.status(500).
                res.send({
                    success: false,
                    error: err
                });
        } else {
            if (user) {
                console.log(user)
                res.status(200)
                res.send({
                    success: true,
                    user: user
                });
            } else {
                res.status(404)
                res.send({
                    success: false,
                    message: 'User not found'
                });
            }
        }
        next();
    });
});



server.post('/update', (req, res, next) => {
    const { walletPubKey, profilePicture, userName, aboutYou, webSite } = req.body;
    User.findOne({ pubKey: walletPubKey }, (err, existingUser) => {
        if (err) {
            res.status(500)
            res.send({
                success: false,
                error: err
            });
        } else {
            if (existingUser) {
                existingUser.profilePicture = profilePicture;
                existingUser.userName = userName;
                existingUser.aboutYou = aboutYou;
                existingUser.webSite = webSite;

                existingUser.save((err) => {
                    if (err) {
                        res.status(500);
                        res.send({
                            success: false,
                            error: err
                        });
                    } else {
                        res.status(202)
                        res.send({
                            success: true
                        });
                    }
                    next();
                });
            } else {
                const newUser = new User({
                    pubKey: walletPubKey,
                    profilePicture: profilePicture,
                    userName: userName,
                    aboutYou: aboutYou,
                    webSite: webSite,
                });

                newUser.save((err) => {
                    if (err) {
                        res.status(500)
                        res.send({
                            success: false,
                            error: err
                        });
                    } else {
                        res.status(202)
                        res.send({
                            success: true
                        });
                    }
                    next();
                });
            }
        }
    });
});



server.listen(8080, () => {
    console.log('Server is listening on port 8080');
});
