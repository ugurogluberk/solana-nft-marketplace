import 'dart:io';

import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:image_picker/image_picker.dart';

class ProfileService {
  static const String baseUrl = 'http://10.0.2.2:8080/update';
  static const String baseUrlToGet =
      'http://10.0.2.2:8080/get/EX345754892063456789098432';

  static Future<bool> createOrUpdateProfile(File profilePicture,
      String userName, String aboutYou, String webSite) async {
    // convert profile picture to base64 string
    List<int> imageBytes = await profilePicture.readAsBytes();
    String base64Image = base64Encode(imageBytes);

    final Map<String, dynamic> data = {
      'walletPubKey': "EX345754892063456789098432",
      'profilePicture': base64Image,
      'userName': userName,
      'aboutYou': aboutYou,
      'webSite': webSite,
    };

    final http.Response response = await http.post(
      Uri.parse(baseUrl),
      body: json.encode(data),
      headers: {'Content-Type': 'application/json'},
    );
    if (response.statusCode != 200) {
      return Future.value(false);
    }
    return Future.value(true);
  }

  static Future<Map<String, String>> getProfileData() async {
    final Uri url = Uri.parse(baseUrlToGet);

    try {
      final http.Response response =
          await http.get(url, headers: {'Content-Type': 'application/json'});
      if (response.statusCode == 200) {
        // Başarılı yanıt alındı, kullanıcı verilerini işle
        final responseData = json.decode(response.body);
        Map<String, String> userData = {
          'userName': responseData['user']['userName'],
          'aboutYou': responseData['user']['aboutYou']
        };

        // İşlemler tamamlandıktan sonra true dön
        return Future.value(userData);
      } else {
        // İstek başarısız oldu, hata mesajını yazdır
        print(response.body);
        return Future.value(null);
      }
    } catch (error) {
      // Hata oluştu, hata mesajını yazdır
      print(error);
      return Future.value(null);
    }
  }
}
