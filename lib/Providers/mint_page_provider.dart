import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final pageViewControllerProvider = StateProvider((ref) => PageController());
final mintedImageProvider = StateProvider((ref) => File(""));
final itemNameProvider = StateProvider((ref) => "Give a name");
final itemDescriptionProvider = StateProvider((ref) => "A brief description");
